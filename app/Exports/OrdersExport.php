<?php

namespace App\Exports;

use App\Models\Orders\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class OrdersExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct($start_date, $end_date,$status = null)
    {
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->status = $status;

    }

    public function query()
    {

        $query = Order::select('orders.consumer_name','orders.quantity','orders.total_price', 'orders.discounted_price', 'orders.end_price','order_status.name','orders.created_at' )
        ->join('order_status', function ($join) {
            $join->on('orders.order_statu_id',  'order_status.id');
        })
        ->WhereBetween('orders.created_at', [$this->start_date, $this->end_date]);   
        if($this->status){
            $query->where('orders.order_statu_id', $this->status);
        }
        return $query;
     }

    public function headings(): array
    {
        return [
            'NOMBRE CLIENTE',
            'CANTIDAD DE PORDUCTOS',
            'PRECIO',
            'DESCUENTO',
            'PRECIO FINAL',
            'ESTADO',
            'FECHA DE CREACIÓN',
        ];
    }
}
