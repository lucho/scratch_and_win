<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;

use App\Models\User;

class Login extends Component
{
    /**
     * Variables
     */
    public $email = '';
    public $password = '';
    public $remember_me = false;
    
    /**
     * Definición de validaciones
     */
    protected $rules = [
        'email' => 'required|email:rfc,dns',
        'password' => 'required'
    ];

    /**
     * Asignación de valores por defecto
     */
    public function mount() {
        if(auth()->user()) {
            return redirect()->intended('/home');      
        }
        $this->fill(['email' => 'admin@piccolino.com', 'password' => 'piccolino2021']);
    }

    /************************************
     *           Funciones              *  
     ************************************/

     /**
      * Valida el ingreso al sistema
      */
    public function login() {
        $credentials = $this->validate();

        if(auth()->attempt(['email' => $this->email, 'password' => $this->password], $this->remember_me)) {
            $user = User::where(["email" => $this->email])->first();
            auth()->login($user, $this->remember_me);
            return redirect()->intended('/home');        
        }
        else{
            return $this->addError('email', trans('auth.failed')); 
        }
    }

    /**
     * Imprime la vista
     */

    public function render()
    {
        return view('livewire.auth.login');
    }
}
