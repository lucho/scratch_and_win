<?php

namespace App\Http\Livewire\Campaigns\Campaign;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use App\Models\Campaign\{Campaign};

class CampaignCreate extends Component
{
    /**
     * Se declaran las variables que 
     * tendran los datos que digiten en 
     * el formulario
     */
    public $name = '';
    public $start_date = '';
    public $end_date = '';
   
    



    /**
     * Añade la Campaña
     */
    public function addCampaign()
    {
        /**
         * Validamos los campos requeridos
         */
        $validatedData = $this->validate([
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);
       
        /**
         * Registramos el cupón
         */
        $data = [
            'name' => $this->name,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
        ];
        Campaign::create($data);
       
        $this->reset(['name', 'start_date', 'end_date']);
        session()->flash('succes', 'El registro se ha añadido con éxito.');
        return redirect(route('campaign-list'));
    }

    

    public function render()
    {
        return view('livewire.campaigns.campaign.campaign-create');
    }
}
