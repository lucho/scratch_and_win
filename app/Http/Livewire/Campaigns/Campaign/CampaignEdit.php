<?php

namespace App\Http\Livewire\Campaigns\Campaign;

use Livewire\Component;
use App\Models\Campaign\{Campaign};
class CampaignEdit extends Component
{


    public Campaign $campaign;
   
   

    

    /**
     * Validamos que los datos del objeto sean los requeridos
     */
    public function rules()
    {
        return [
            'campaign.name' => 'required',
            'campaign.start_date' => 'required',
            'campaign.end_date' => 'required',

        ];
    }

   

    /**
     * Carga con valores las
     * variables
     */
    public function mount($id)
    {
        if ((auth()->user()->isAdmin())) {
            $this->campaign = campaign::find($id);
            $this->campaign->start_date = \Carbon\Carbon::parse($this->campaign->start_date)->format('Y-m-d');
            $this->campaign->end_date = \Carbon\Carbon::parse($this->campaign->end_date)->format('Y-m-d');
        } else {
            redirect('404');
        }
    }

      
    
    /**
     * editCampaign
     * Edita la campaña
     * @return void
     */
    public function editCampaign()
    {
        
        $existingCampaign = Campaign::find($this->campaign->id);
        if ($existingCampaign) {
            /**
             * Registramos el cupón
             */
            $data = [
                'name' => $this->campaign->name,
                'start_date' => $this->campaign->start_date,
                'end_date' => $this->campaign->end_date,
            ];
            
            
            $existingCampaign->update($data);
           
            session()->flash('succes', 'Su campaña ha sido editada.');
            return redirect(route('campaign-list'));
        } else {
            session()->flash('failure', 'Esta campaña no existe');
        }
    }
  
    public function render()
    {
        return view('livewire.campaigns.campaign.campaign-edit');
    }
}
