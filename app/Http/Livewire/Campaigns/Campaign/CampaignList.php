<?php

namespace App\Http\Livewire\Campaigns\Campaign;

use Livewire\Component;
use App\Models\Campaign\Campaign;
use Livewire\WithPagination;

class CampaignList extends Component
{
    use WithPagination;

    public Campaign $campaign;

    public $showSuccesNotification = false;
    public $showFailureNotification = false;

    public $search = '';
    public $sortField = 'id';
    public $sortDirection = 'asc';
    public $perPage = 10;

    protected $queryString = ['sortField', 'sortDirection'];
    protected $paginationTheme = 'bootstrap';

    public function sortBy($field){
        if($this->sortField === $field) {
            $this->sortDirection = $this->sortDirection === 'asc' ? 'desc' : 'asc';
        } else {
            $this->sortDirection = 'asc';
        }
        $this->sortField = $field;
    }

    public function delete($id) {
        $this->campaign = Campaign::find($id);
        if(auth()->user()->isAdmin()) {
            $this->campaign->delete();
            $this->showSuccesNotification = true;
        }
        else {
            $this->showFailureNotification = true;
        }
    }
    public function render()
    {
        return view('livewire.campaigns.campaign.campaign-list',[
            'campaigns' => Campaign::generalSearch($this->search)->where('enabled', 1)->orderBy($this->sortField, $this->sortDirection)->paginate($this->perPage)
        ]);
    }

  
}
