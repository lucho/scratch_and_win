<?php

namespace App\Http\Livewire\Campaigns\Ticket;

use Livewire\Component;

use App\Models\Campaign\{Campaign,Ticket};
use Carbon\Carbon;

class TicketCreate extends Component
{
  
    public $code = '';
    public $donation = '';
    public $award = '';
    public $campaign_id = '';

    
    
    /**
     * Se declaran las variables 
     * que tendran las listas para 
     * los Selects
     */
    public $campaigns;
   

   
    /**
     * Carga con valores las
     * variables
     */
    public function mount()
    {
        $this->campaigns = Campaign::where('enabled', 1)
        ->where('start_date', '<=', Carbon::now())
            ->where('end_date', '>=', Carbon::now())
            ->get();
    }

    /**
     * Añade el Ticket
     */
    public function addTicket()
    {
        /**
         * Validamos los campos requeridos
         */
        $validatedData = $this->validate([
            'code' => 'required',
            'donation' => 'required',
            'campaign_id' => 'required',

        ]);
       
        /**
         * Obtenemos el ID de la campaña
         */
        if (is_array($this->campaign_id) && array_key_exists("value", $this->campaign_id)) {
            $this->campaign_id = intval($this->campaign_id['value']);
        }
        /**
         * Registramos el cupón
         */
        $data = [
            'code' => $this->code,
            'donation' => $this->donation,
            'award' => $this->award,
            'campaign_id' => $this->campaign_id,
            'user_at' => auth()->user()->id
        ];

       
        
       Ticket::create($data);

       

       
        $this->reset(['code', 'donation', 'award', 'campaign_id']);
        session()->flash('succes', 'El registro se ha añadido con éxito.');
        return redirect(route('campaign-ticket-list'));
    }

   

    public function render()
    {
        return view('livewire.campaigns.ticket.ticket-create');
    }
}
