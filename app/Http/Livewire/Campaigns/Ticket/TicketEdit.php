<?php

namespace App\Http\Livewire\Campaigns\Ticket;

use Livewire\Component;

use App\Models\Campaign\{Campaign,Ticket};
use Carbon\Carbon;
class TicketEdit extends Component
{


    public Ticket $ticket;
    public $campaign_id = '';

   

    /**
     * Se declaran las variables 
     * que tendran las listas para 
     * los Selects
     */
    public $campaigns;
   
   
    /**
     * Validamos que los datos del objeto sean los requeridos
     */
    public function rules()
    {
        return [
            'ticket.code' => 'required',
            'ticket.donation' => 'required',
            'ticket.award' => '',
            'ticket.campaign_id' => 'required'
        ];
    }

   
   

    /**
     * Carga con valores las
     * variables
     */
    public function mount($id)
    {
        if ((auth()->user()->isAdmin())) {
            $this->ticket = Ticket::find($id);
            $this->campaigns = Campaign::where('enabled', 1)
            ->where('start_date', '<=', Carbon::now())
                ->where('end_date', '>=', Carbon::now())
                ->get();
                $this->campaign_id = $this->ticket->campaign_id;
        } else {
            redirect('404');
        }
    }

      
     
    /**
     * editTicket
     * Edita el Ticket
     * @return void
     */
    public function editTicket()
    {
        
        $existingTicket = Ticket::find($this->ticket->id);
        if ($existingTicket) {
            /**
             * Registramos el cupón
             */
            /**
         * Obtenemos el ID de la campaña
         */
        if (is_array($this->campaign_id) && array_key_exists("value", $this->campaign_id)) {
            $this->campaign_id = intval($this->campaign_id['value']);
        }
            $data = [
                'code' => $this->ticket->code,
                'donation' => $this->ticket->donation,
                'award' => $this->ticket->award,
                'campaign_id' => $this->campaign_id

            ];

          
            $existingTicket->update($data);
            
            session()->flash('succes', 'Su Ticket ha sido editado.');
            return redirect(route('campaign-ticket-list'));
        } else {
            session()->flash('failure', 'Este Ticket no existe');
        }
    }
    

    public function render()
    {
        return view('livewire.campaigns.ticket.ticket-edit');
    }
}
