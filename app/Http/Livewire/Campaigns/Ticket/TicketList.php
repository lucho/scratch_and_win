<?php

namespace App\Http\Livewire\Campaigns\Ticket;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Campaign\Ticket;
class TicketList extends Component
{
    use WithPagination;

    public Ticket $ticket;

    public $showSuccesNotification = false;
    public $showFailureNotification = false;

    public $search = '';
    public $sortField = 'id';
    public $sortDirection = 'asc';
    public $perPage = 20;

    protected $queryString = ['sortField', 'sortDirection'];
    protected $paginationTheme = 'bootstrap';

    public function sortBy($field){
        if($this->sortField === $field) {
            $this->sortDirection = $this->sortDirection === 'asc' ? 'desc' : 'asc';
        } else {
            $this->sortDirection = 'asc';
        }
        $this->sortField = $field;
    }

    public function delete($id) {
        $this->ticket = Ticket::find($id);
        if(auth()->user()->isAdmin()) {
            $this->ticket->delete();
            $this->showSuccesNotification = true;
        }
        else {
            $this->showFailureNotification = true;
        }
    }

  
    public function render()
    {

        return view('livewire.campaigns.ticket.ticket-list',[
                     'tickets' => Ticket::with('campaign')->generalSearchMultipleItems($this->search)->where('enabled', 1)->orderBy($this->sortField, $this->sortDirection)->paginate($this->perPage)
             ]);
    }

 
}
