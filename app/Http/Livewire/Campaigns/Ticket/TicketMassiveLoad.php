<?php

namespace App\Http\Livewire\Campaigns\Ticket;

use Livewire\WithFileUploads;

use Livewire\Component;
use App\Imports\TicketImport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel as MaatExcel;

class TicketMassiveLoad extends Component
{
    use WithFileUploads;

    public $upload = '';

    /**
     * Añade el Ticket
     */
    public function addFile()
    {

        $this->validate([
            'upload' => 'required',
        ]);
        try {
            DB::beginTransaction();
            MaatExcel::import(new TicketImport, $this->upload);
            DB::commit();
            $this->reset(['upload']);
            session()->flash('succes', 'El archivo se ha añadido con éxito.');
            return redirect(route('campaign-ticket-list'));
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            dd('Aqui error', $failures);
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('failure', $e->getMessage());
            return redirect(route('campaign-ticket-massive-load'));
        }
    }



    public function render()
    {
        return view('livewire.campaigns.ticket.ticket-massive-load');
    }
}
