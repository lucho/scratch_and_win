<?php

namespace App\Http\Livewire\Dashboard;

use Illuminate\Support\Facades\DB;

use Livewire\Component;
//use App\Models\Orders\{Order, PlaneOrder, CouponAppliedOrder, SortedDishe};
use App\Models\User;
use Carbon\Carbon;

class DashboardDefault extends Component
{

    /**
     * Controla los totales 
     * de ordenes del día
     */
    public $usersTotal = 0;
    public $registered = 0;
    public $inProcess = 0;
    public $completed = 0;
    public $ordersTotal = 0;

    /**
     * Para la grafica de 
     * ventas por mes del año
     */

    public $january = 0;
    public $february = 0;
    public $march = 0;
    public $april = 0;
    public $may = 0;
    public $june = 0;
    public $july = 0;
    public $august = 0;
    public $september = 0;
    public $october = 0;
    public $november = 0;
    public $december = 0;

    /**
     * Usuarios registrados por mes
     */
    public $januaryUser = 0;
    public $februaryUser = 0;
    public $marchUser = 0;
    public $aprilUser = 0;
    public $mayUser = 0;
    public $juneUser = 0;
    public $julyUser = 0;
    public $augustUser = 0;
    public $septemberUser = 0;
    public $octoberUser = 0;
    public $novemberUser = 0;
    public $decemberUser = 0;

    /**
     * Carga con valores las
     * variables
     */
    public function mount()
    {
        $this->loadData();
    }

    /**
     * render
     *
     * @return void
     */
    public function render()
    {
        return view('livewire.dashboard.dashboard-default');
    }


    /**
     * loadData
     * Realiza la consulta de los datos
     * necesarios del home
     * @return void
     */
    public function loadData()
    {
        // //dd(Carbon::now()->format('m'));
        // $this->usersTotal = User::where('role_id', 2)->count();
        // $this->registered = count(Order::where('order_statu_id', 1)->get());
        // $this->inProcess = count(Order::where('order_statu_id', 2)->get());
        // $this->completed = count(Order::where('order_statu_id', 3)->get());
        // /**
        //  * Valores para la
        //  * grafica de ventas por mes
        //  */
        // $this->january = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "01")->sum(DB::raw("end_price"));
        // $this->february = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "02")->sum(DB::raw("end_price"));
        // $this->march = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "03")->sum(DB::raw("end_price"));
        // $this->april = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "04")->sum(DB::raw("end_price"));
        // $this->may = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "05")->sum(DB::raw("end_price"));
        // $this->june = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "06")->sum(DB::raw("end_price"));
        // $this->july = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "07")->sum(DB::raw("end_price"));
        // $this->august = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "08")->sum(DB::raw("end_price"));
        // $this->september = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "09")->sum(DB::raw("end_price"));
        // $this->october = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "10")->sum(DB::raw("end_price"));
        // $this->november = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "11")->sum(DB::raw("end_price"));
        // $this->december = Order::where('order_statu_id', 3)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "12")->sum(DB::raw("end_price"));
        /**
         * Para la grafica de usuarios
         * registrados por mes
         */
        $this->januaryUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "01")->count();
        $this->februaryUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "02")->count();
        $this->marchUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "03")->count();
        $this->aprilUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "04")->count();
        $this->mayUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "05")->count();
        $this->juneUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "06")->count();
        $this->julyUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "07")->count();
        $this->augustUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "08")->count();
        $this->septemberUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "09")->count();
        $this->octoberUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "10")->count();
        $this->novemberUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "11")->count();
        $this->decemberUser = User::where('role_id', 2)->whereYear('created_at', Carbon::now()->format('Y'))->whereMonth('created_at', "12")->count();
        // $this->ordersTotal = count(Order::get());
    }
}
