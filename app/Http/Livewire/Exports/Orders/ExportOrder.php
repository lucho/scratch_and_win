<?php

namespace App\Http\Livewire\Exports\Orders;

use Livewire\Component;
use App\Models\Orders\{OrderStatu};
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OrdersExport;
use Carbon\Carbon;

class ExportOrder extends Component
{

    /**
     * Se declaran las variables que 
     * tendran los datos que digiten en 
     * el formulario
     */
    public $start_date = '';
    public $end_date = '';
    public $statusSelect = null;

    /**
     * Variable para el select
     * de estados
     */
    public $orderStatus = null;

    /**
     * Carga con valores las
     * variables
     */
    public function mount()
    {
        if ((auth()->user()->isAdmin())) {
            $this->orderStatus = OrderStatu::where('enabled', 1)->get();
        }
    }

    /**
     * Exporta el archivo
     */
    public function export()
    {
        /**
         * Validamos los campos requeridos
         */
        $this->validate([
            'start_date' => 'required',
            'end_date' => 'required'
        ]);
        try {
            session()->flash('succes', 'Reporte creado con éxito.');
            return Excel::download(new OrdersExport($this->start_date,$this->end_date,$this->statusSelect), Carbon::now()->format('Y-m-d'). '-ordenes.xlsx');
        } catch (\Exception $e) {
            session()->flash('failure', $e->getMessage());
        }
    }

    public function render()
    {
        return view('livewire.exports.orders.export-order');
    }
}
