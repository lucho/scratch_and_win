<?php

namespace App\Http\Livewire\Players\Player;

use Livewire\Component;

class PlayerView extends Component
{
    public function render()
    {

     return view('livewire.players.player.player-view');
    }
}
