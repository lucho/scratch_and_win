<?php

namespace App\Http\Livewire\Players\Screens;

use Livewire\Component;
use App\Models\Campaign\{ Ticket};
use App\Models\Player\{Player, Donation};

use Carbon\Carbon;
use Illuminate\Http\Request;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;

class ScreenSelectedGroup extends Component
{

    use WithFileUploads;

    /**
     * Datos generales de la donación
     */
    public $tickets = [];
    public $totalValue = null;
    /**
     * Datos del jugador; 
     */
    public  $name = null;
    public  $phone = null;
    public $email = null;



    /**
     * Variable para el cargue de la imagen
     */
    public $upload = '';
    /**
     * Se declaran las variables 
     * que tendran las listas para 
     * los Selects
     */


    /**
     * Setea los mensajes dfe los campos requeridos
     */
    protected $messages = [
        'upload.image' => 'Por favor, cargue una imagen del cupón',
        'upload.max' => 'El tamaño de la imagen es demasiado grande. Asegúrese de que está debajo de 2MB',
    ];

    /**
     * Carga con valores las
     * variables
     */
    public function mount(Request $request)
    {
        // dd($request->all());
        $this->tickets = Ticket::with('campaign')->where('enabled', 1)->whereIn('id', explode(',', $request['iC']))
            ->WhereHas('campaign', function ($query) {
                return $query->where('enabled', 1)->where('start_date', '<=', Carbon::now())
                    ->where('end_date', '>=', Carbon::now());
            })->get();
        $tickets = [];
        foreach ($this->tickets as $key => $ticket) {
            $this->totalValue = ($this->totalValue + $ticket->donation);
            array_push($tickets, $ticket->id);

            # code...
        }
        $this->tickets = $tickets;
        //  dd('Hola', $this->tickets);
    }


    /**
     * Añade una donación
     */
    public function addDonation()
    {
        /**
         * Validamos los campos requeridos
         */
        $validatedData = $this->validate([
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'upload' => 'required'

        ]);
        try {
            /**
             * Registramos el jugador
             */
            $dataPlayer = [
                'name' => $this->name,
                'phone' => $this->phone,
                'email' => $this->email
            ];
            /**
             * 
             */

            DB::beginTransaction();

            $player =  Player::where('email', $this->email)->first();
            if (!$player) {
                $player = Player::create($dataPlayer);
            }

            $dataDonation = [
                "player_id" => $player->id,
                "quantity" =>  count($this->tickets),
                "donation_total" =>  $this->totalValue,
                'image' => $this->upload->store('/', 'public')
            ];

            $donation = Donation::create($dataDonation);
            $donation->tickets()->attach($this->tickets);
            foreach ($this->tickets as $key => $ticket) {
                $ticket= Ticket::find($ticket);
                if($ticket){
                    $ticket->update(['enabled' => 0]);
                }
            }
            /**
             * Dejamos las variables globales
             * de nuevo sin datos
             */
            DB::commit();

            $this->reset(['name', 'phone', 'email']);
            session()->flash('succes', 'El registro se ha añadido con éxito.');
            return redirect(route('player'));
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());
            return redirect(route('player'));
        }
    }







    public function render()
    {
        return view('livewire.players.screens.screen-selected-group');
    }
}
