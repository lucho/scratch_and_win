<?php

namespace App\Http\Livewire\Players\Screens;

use Livewire\Component;
use App\Models\Campaign\{Campaign,Ticket};
use Carbon\Carbon;

class ScreenTicketSelection extends Component
{
   /**
     * Datos generales de la donación
     */
    public $tickets = [];
  
   /**
    * Carga con valores las
    * variables
    */
   public function mount()
   {
       $this->tickets = Ticket::with('campaign')->where('enabled',1)
       ->WhereHas('campaign', function ($query)  {
           return $query->where('enabled', 1)->where('start_date', '<=', Carbon::now())
           ->where('end_date', '>=', Carbon::now());
       })->get()->random(12);

   }


   

    public function render()
    {
        return view('livewire.players.screens.screen-ticket-selection');
    }
}
