<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(["status" => "ERROR", "message" => 'Token is invalid','code'=>'AUTHORIZATIONNOTFOUND'], 423);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(["status" => "ERROR", "message" => 'Token is Expired','code'=>'AUTHORIZATIONNOTFOUND'], 423);
            }else{
                return response()->json(["status" => "ERROR", "message" => 'Authorization Token not found','code'=>'AUTHORIZATIONNOTFOUND'], 423);
            }
        }
        return $next($request);
    }
}
