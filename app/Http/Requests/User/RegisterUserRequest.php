<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
class RegisterUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
  
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => "required",
            'name' => "required",
            'email' => "required|email|unique:users",
            'password' => "required",
            'role_id' => "required",
        ];
    }
  
  
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "avatar" => "El :attribute es obligatorio",
            "name" => "El :attribute es obligatorio",
            "email" => "El :attribute es obligatorio",
            "password" => "El :attribute es obligatorio",
            "role_id" => "El :attribute es obligatorio"
        ];
    }
  
    /**
     * Valida que los Campos sean los requeridos
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $mytime = Carbon::now();
        $response = response()->json([
            "message" => [$validator->errors()],
            "date" => str_replace(" ", "T", $mytime->toDateTimeString())
        ], 422);
        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
  
    /**
     * Error en caso de no tener autorización en el sistema
     */
    protected function failedAuthorization()
    {
        $response = response()->json([
            'message' => 'No tiene Accesos permitido',
            'code' => 422
        ], 422);
        throw new \Illuminate\Auth\Access\AuthorizationException($response);
    }
 
}
