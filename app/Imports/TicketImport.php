<?php

namespace App\Imports;

use App\Models\Campaign\Ticket;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\{WithHeadingRow, WithValidation, WithBatchInserts, WithChunkReading};
class TicketImport implements ToModel, WithHeadingRow, WithValidation
{

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Ticket([
            'code' => $row['code'],
            'donation' => $row['donation'],
            'campaign_id' => $row['campaign'],
            'user_at' => auth()->user()->id
        ]);
    }



    public function rules(): array
    {

        return [
            "campaign" => ['exists:campaigns,id']
        ];
    }


    // public function batchSize(): int
    // {
    //     return 1000;
    // }


    // public function chunkSize(): int
    // {
    //     return 100;
    // }
}
