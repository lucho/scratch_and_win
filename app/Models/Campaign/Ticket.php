<?php

namespace App\Models\Campaign;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Campaign\Campaign;

class Ticket extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'code',
        'donation',
        'award',
        'campaign_id',
        'user_at',
        'enabled'
    ];

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

}
