<?php

namespace App\Models\Player;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Campaign\Ticket;

class Donation extends Model
{
    use HasFactory;
    use HasFactory;

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'player_id',
        'quantity',
        'image',
        'donation_total'
    ];

    public function tickets()
    {
        return $this->belongsToMany(Ticket::class)->withTimestamps();
    }
}
