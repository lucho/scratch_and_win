<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;


    /********************************
     *  Policia del administardor   *
     ********************************/

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * adminEdit
     * Valida que el usaurio tenga permisos
     * de edición
     * @param  mixed $user
     * @return void
     */
    public function adminEdit(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * adminCreate
     * Valida que el usuario tenga permisos de 
     * creacion
     * @param  mixed $user
     * @return void
     */
    public function adminCreate(User $user)
    {
        return $user->isAdmin();
    }
    /**
     * adminDelete
     * Valida que el usaurio tenga permisos de 
     * Eliminar
     * @param  mixed $user
     * @return void
     */
    public function adminDelete(User $user)
    {
        return $user->isAdmin();
    }


    /**
     * userIsAdministrator
     * Valida que el usaurio sea administrador
     * @param  mixed $user
     * @return void
     */
    public function userIsAdministrator(User $user)
    {
        return $user->isAdmin();
    }
}
