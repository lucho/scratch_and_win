<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Role;
use App\Models\Tag;
use App\Models\Category;

use Livewire\Component;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       
        /**********************************************
         *    Se crean las consultas personalizadas   *
         **********************************************/


        /**
         * Buscador general de las tablas
         * Consulta por
         * ID,NAME,CREATED_AT
         */
        Builder::macro('generalSearch', function ($string) {
            if ($string) {
                return $this->where('id', '=', intval($string))
                    ->orWhere('name', 'like', '%' . $string . '%')
                    ->orWhere('created_at', 'like', '%' . $string . '%');
            } else {
                return $this;
            }
        });


        /**
         * Buscador general, de las consulta multi tablas de las tablas
         * Consulta por
         * ID,NAME,DESCRIPTION,CREATED_AT
         */
        Builder::macro('generalSearchMultipleItems', function ($string) {
            if ($string) {
                return $this->where('items.id', '=', intval($string))
                    ->orWhere('items.name', 'like', '%' . $string . '%')
                    ->orWhere('items.description', 'like', '%' . $string . '%')
                    ->orWhere('items.created_at', 'like', '%' . $string . '%');
            } else {
                return $this;
            }
        });

        /**
         * Consulta para 
         * las ordenes
         */
        Builder::macro('generalOrder', function ($string) {
            if ($string) {
                return $this->where('id', '=', intval($string))
                    ->orWhere('consumer_name', 'like', '%' . $string . '%')
                    ->orWhere('created_at', 'like', '%' . $string . '%');
            } else {
                return $this;
            }
        });
    }
}
