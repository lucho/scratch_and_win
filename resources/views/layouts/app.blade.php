<x-layouts.base>
    @auth()
        {{-- Si un Administrador es autenticado --}}
        @if (auth()->user()->isAdmin())
          
                @include('layouts.navbars.auth.sidebar')
                @include('layouts.navbars.auth.topnav')
                @include('components.plugins.fixed-plugin')
                <main class="main-content mt-1 border-radius-lg">
                    {{ $slot }}
                </main>
                @include('layouts.footers.auth')
        @endif
    @endauth

    {{-- Si el usuario no está autenticado (si el usuario es un invitado)--}}
    @guest
        {{-- Si la usuario está en la página de inicio de sesión --}}
        @if (!auth()->check() && in_array(request()->route()->getName(),['login'],))
            @include('layouts.navbars.guest.dark-nav')
            {{ $slot }}
            @include('layouts.footers.guest')
        @endif
        @if (!auth()->check() && in_array(request()->route()->getName(),['player','screen-ticket-selection','screen-selected-group'],))
            {{ $slot }}
        @endif
    @endguest

</x-layouts.base>

