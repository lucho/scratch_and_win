<main class="main-content  mt-0">
    <div class="page-header align-items-start section-height-50 pt-5 pb-11 m-3 border-radius-lg"
        style="background-image: url('../../../assets/company/poster4.png');">
        <span class="mask bg-gradient-dark opacity-6"></span>
        <div class="container">
            <div class="row d-flex flex-column justify-content-center">
                <div class="col-lg-5 text-center mx-auto">
                    <h1 class="text-white mb-2 mt-5">{{ __('Ágapē!') }}</h1>
                    <p class="text-lead text-white">
                        {{ __('ágapē  es un sistema para restaurantes enfocado en la gestión de los pedidos, promociones, reportes entre otros..') }}
                    </p>
                </div>
               
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-lg-n10 mt-md-n11 mt-n10 justify-content-center">
            <div class="col-xl-4 col-lg-5 col-md-7 mx-auto">
                <div class="card z-index-0">
                    <div class="card-header text-center pt-4">
                        <h5>{{ __('Iniciar sesión') }}</h5>
                    </div>
                    <div class="row px-xl-5 px-sm-4 px-3">
                    
                    </div>
                    <div class="card-body">
                        <form wire:submit.prevent="login" action="#" metohd="POST" role="form text-start">
                            <div>
                                <div class="@error('email')has-danger @enderror">
                                    <input wire:model="email" type="email"
                                        class="form-control @error('email')is-invalid @enderror" placeholder="Correo"
                                        aria-label="Email" aria-describedby="email-addon">
                                </div>
                                @error('email') <div class="text-danger text-xs">{{ $message }}</div> @enderror
                            </div>
                            <div class="mt-3">
                                <div class="@error('password')has-danger @enderror">
                                    <input wire:model="password" type="password"
                                        class="form-control @error('password')is-invalid @enderror"
                                        placeholder="Contraseña" aria-label="Password" aria-describedby="password-addon">
                                </div>
                                @error('password') <div class="text-danger text-xs">{{ $message }}</div> @enderror
                            </div>
                            <div class="form-check form-switch mt-4">
                                <input wire:model="remember_me" class="form-check-input" type="checkbox"
                                    id="rememberMe">
                                <label class="form-check-label" for="rememberMe">{{ __('Recordarme') }}</label>
                            </div>
                            <div class="text-center">
                                <button type="submit"
                                    class="btn bg-gradient-info w-100 my-4 mb-2">{{ __('Ingresar') }}</button>
                            </div>
                            <div class="mb-2 position-relative text-center">
                                <p
                                    class="text-sm font-weight-bold mb-2 text-secondary text-border d-inline z-index-2 bg-white px-3">
                                    {{ __('or') }}
                                </p>
                            </div>
                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
