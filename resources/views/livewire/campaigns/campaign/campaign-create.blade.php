<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <!--progress bar-->


            <!--form panels-->
            <div class="row">
                <div class="col-12 col-lg-8 m-auto">
                    <form wire:submit.prevent="addCampaign" class="multisteps-form__form mb-8">
                        <!--single form panel-->
                        <div class="card multisteps-form__panel p-3 border-radius-xl bg-white js-active" data-animation="FadeIn">
                            <h5 class="font-weight-bolder">{{ __('Registrar Campaña') }}</h5>
                            <div class="multisteps-form__content">
                                <div class="row mt-3">
                                    <!-- Titulo -->
                                    <div class="col-12 col-sm-6">
                                        <label>{{ __('Nombre') }}</label>
                                        <div class="@error('name')has-danger @enderror">
                                            <input wire:model="name" class="multisteps-form__input form-control @error('name')is-invalid @enderror" type="text" placeholder="Título" />
                                        </div>
                                        @error('name') <div class="text-danger text-xs">
                                            {{ $message }}
                                        </div>@enderror
                                    </div>
                                    <!-- /Titulo -->

                                  

                                    <!-- Fechas -->
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>{{ __('Fecha de inicio') }}</label>
                                            <div class="@error('start_date')has-danger @enderror ">
                                                <input wire:model="start_date" class="multisteps-form__input form-control @error('start_date')is-invalid @enderror datetimepicker" type="text" placeholder="Fecha de inicio" />
                                            </div>
                                            @error('start_date') <div class="text-danger text-xs">
                                                {{ $message }}
                                            </div>@enderror
                                        </div>
                                        <div class="col-sm-6">
                                            <label>{{ __('Fecha de finalizacion') }}</label>
                                            <div class="@error('end_date')has-danger @enderror ">
                                                <input wire:model="end_date" class="multisteps-form__input form-control @error('end_date')is-invalid @enderror datetimepicker" type="text" placeholder="Fecha de finalizacion" />
                                            </div>
                                            @error('end_date') <div class="text-danger text-xs">
                                                {{ $message }}
                                            </div>@enderror
                                        </div>


                                    </div>
                                    <!-- /Fechas -->

                                   






                                   
                                    
                                   
                                    <div class="button-row d-flex mt-4">
                                        <button class="btn bg-gradient-dark ms-auto mb-0" type="submit" title="Send">{{ __('Guardar') }}</button>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--   Core JS Files   -->

<script src="../../../assets/js/plugins/choices.min.js"></script>
<script src="../../../assets/js/plugins/flatpickr.min.js"></script>
<script>
  
    if (document.querySelector('.datetimepicker')) {
        flatpickr('.datetimepicker', {
            allowInput: true
        }); // flatpickr
    }
</script>

