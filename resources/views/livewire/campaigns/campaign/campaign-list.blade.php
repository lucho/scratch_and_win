<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div>
                    @if ($showSuccesNotification)
                    <div class="mt-3 alert alert-primary alert-dismissible fade show" role="alert">
                        <span class="alert-icon text-white"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text text-white">
                            {{ __('El registro ha sido eliminado con éxito.!') }}</span>
                        <button wire:click="$set('showSuccesNotification', false)" type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                </div>
                <div>
                    @if (session('succes'))
                    <div class="mt-3 alert alert-primary alert-dismissible fade show" role="alert">
                        <span class="alert-icon text-white"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text text-white">{{ session('succes') }}</span>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if (session('failure'))
                    <div class="mt-3 alert alert-primary alert-dismissible fade show" role="alert">
                        <span class="alert-text text-white">{{ session('failure') }}</span>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                        </button>
                    </div>
                    @endif
                </div>
                <div class="d-flex flex-column mx-3 mt-3">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h5 class="mb-0">{{ __('Lista') }}</h5>
                        </div>
                        @can('admin-create', auth()->user())
                        <a href="{{ route('campaign-create') }}" class="btn bg-gradient-primary btn-sm mb-0" type="button">+&nbsp;{{ __('Nueva Campaña') }}</a>
                        @endcan
                    </div>
                    <div class="d-flex flex-row justify-content-between">
                        <div class="d-flex mt-3 align-items-center justify-content-center">
                            <p class="text-secondary pt-2">{{ __('Mostrar') }}&nbsp;&nbsp;</p>
                            <select wire:model="perPage" class="form-control" id="entries">
                                <option value="5">{{ __('5') }}</option>
                                <option selected value="10">{{ __('10') }}</option>
                                <option value="15">{{ __('15') }}</option>
                                <option value="20">{{ __('20') }}</option>
                            </select>
                            <p class="text-secondary pt-2">&nbsp;&nbsp;{{ __('entradas') }}</p>
                        </div>
                        <div class="mt-1 col-8 ">
                            <input wire:model="search" type="text" class="form-control" placeholder="Buscar...">
                        </div>
                    </div>
                </div>
                <x-table>
                    <x-slot name="head">
                        <x-table.heading sortable wire:click="sortBy('id')" :direction="$sortField === 'id' ? $sortDirection : null">{{ __('ID') }}
                        </x-table.heading>
                        <x-table.heading>{{ __('Imagen') }}</x-table.heading>

                        <x-table.heading sortable wire:click="sortBy('name')" :direction="$sortField === 'name' ? $sortDirection : null">{{ __('Nombre') }}
                        </x-table.heading>
                        <x-table.heading sortable wire:click="sortBy('start_date')" :direction="$sortField === 'start_date' ? $sortDirection : null">{{ __('Fecha de inicio') }}
                        </x-table.heading>
                        <x-table.heading sortable wire:click="sortBy('end_date')" :direction="$sortField === 'end_date' ? $sortDirection : null">{{ __('Fecha de finalización') }}
                        </x-table.heading>
                        <x-table.heading sortable wire:click="sortBy('created_at')" :direction="$sortField === 'created_at' ? $sortDirection : null">
                            {{ __('Fecha de creación') }}
                        </x-table.heading>
                        @can('user-is-administrator', auth()->user())
                        <x-table.heading>{{ 'Opciones' }}</x-table.heading>
                        @endcan
                    </x-slot>

                    <x-slot name="body">
                        @foreach ($campaigns as $item)
                        <x-table.row wire:key="row-{{ $item->id }}">
                            <x-table.cell>{{ $item->id }}</x-table.cell>
                           
                            <x-table.cell>{{ $item->name  }}</x-table.cell>
                            <x-table.cell>{{ \Carbon\Carbon::parse($item->start_date)->format('Y-m-d') }}</x-table.cell>
                            <x-table.cell>{{ \Carbon\Carbon::parse($item->end_date)->format('Y-m-d') }}</x-table.cell>

                            <x-table.cell>{{ $item->created_at }}</x-table.cell>
                            <x-table.cell>
                                @can('user-is-administrator', auth()->user())
                                @can('admin-edit', auth()->user())
                                <a href="{{ route('campaign-edit', ['id' => $item->id]) }}" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Editar">
                                    <i class="fas fa-user-edit text-secondary"></i>
                                </a>
                                @endcan
                                @can('admin-delete', auth()->user())
                                <span>
                                    <i onclick="confirm('¿Está seguro de que desea eliminar este registro ?') || event.stopImmediatePropagation()" wire:click="delete({{ $item->id }})" class="cursor-pointer fas fa-trash text-secondary"></i>
                                </span>
                                @endcan
                                @else
                                <a href="#" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Disabled">
                                    <i class="fas fa-user-edit text-secondary"></i>
                                </a>
                                <span>
                                    <i class="cursor-pointer fas fa-trash text-secondary" data-bs-toggle="tooltip" data-bs-original-title="Disabled"></i>
                                </span>
                                @endcan
                            </x-table.cell>
                        </x-table.row>
                        @endforeach
                    </x-slot>
                </x-table>
                <div id="datatable-bottom">
                    {{ $campaigns->links() }}
                </div>
            </div>
        </div>
    </div>
</div>