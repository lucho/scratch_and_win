<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <!--progress bar-->


            <!--form panels-->
            <div class="row">
                <div class="col-12 col-lg-8 m-auto">
                    <form wire:submit.prevent="editTicket" class="multisteps-form__form mb-8">
                        <!--single form panel-->
                        <div class="card multisteps-form__panel p-3 border-radius-xl bg-white js-active" data-animation="FadeIn">
                            <h5 class="font-weight-bolder">{{ __('Actualizar Ticket') }}</h5>
                            <div class="multisteps-form__content">
                                <div class="row mt-3">
                                    <!-- Código -->
                                    <div class="col-12 col-sm-6">
                                        <label>{{ __('Código') }}</label>
                                        <div class="@error('ticket.code')has-danger @enderror">
                                            <input wire:model="ticket.code" class="multisteps-form__input form-control @error('ticket.code')is-invalid @enderror" type="text" placeholder="Código" />
                                        </div>
                                        @error('ticket.code') <div class="text-danger text-xs">
                                            {{ $message }}
                                        </div>@enderror
                                    </div>
                                    <!-- /Código -->

                                    <!-- Premio -->
                                    <div class="col-12 col-sm-6">
                                        <label>{{ __('Premio') }}</label>
                                        <div class="@error('ticket.award')has-danger @enderror">
                                            <input wire:model="ticket.award" class="multisteps-form__input form-control @error('ticket.award')is-invalid @enderror" type="text" placeholder="Premio" />
                                        </div>
                                        @error('ticket.award') <div class="text-danger text-xs">
                                            {{ $message }}
                                        </div>@enderror
                                    </div>
                                    <!-- /Premio -->
                                     <!-- Precio -->
                                     <div class="col-12 col-sm-6 mt-3 mt-sm-0">
                                            <label>{{ __('Donación') }}</label>
                                            <div class="@error('ticket.donation')has-danger @enderror">
                                                <input wire:model="ticket.donation" class="multisteps-form__input form-control  @error('ticket.donation')is-invalid @enderror" type="number" min="0.00"  step="0.01" placeholder="Donación" />
                                            </div>
                                            @error('ticket.donation') <div class="text-danger text-xs">
                                                {{ $message }}
                                            </div> @enderror
                                        </div>
                                        <!-- /Precio -->

                                
                                    <!-- Campaña -->
                                    <div class="row mt-3">
                                        <div wire:ignore>
                                            <label class="col-12 mt-3 mt-sm-0">{{ __('Campaña') }}</label>
                                            <div class=" @error('campaign_id') has-danger @enderror">
                                                <select wire:model="campaign_id" class="multisteps-form__select form-control @error('campaign_id') is-invalid @enderror" name="choices-multiple-remove-button3"  id="choices-multiple-remove-button3">
                                                    <option selected value="" >{{ __('elegir') }}</option>
                                                    @foreach ($campaigns as $item)
                                                    <option value="{{ $item->id }}" {{ $ticket->campaign_id === $item->id ? 'selected' : ''}}>{{ $item->name }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        @error('campaign_id') <div class="text-danger text-xs text-xs">
                                            {{ $message }}
                                        </div> @enderror
                                    </div>
                                    <!-- /Campaña -->


                                    <div class="button-row d-flex mt-4">
                                        <button class="btn bg-gradient-dark ms-auto mb-0" type="submit" title="Send">{{ __('Actualizar') }}</button>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--   Core JS Files   -->

<script src="../../../assets/js/plugins/choices.min.js"></script>
<script>
    if (document.getElementById('choices-multiple-remove-button3')) {
        var element = document.getElementById('choices-multiple-remove-button3');
        const example = new Choices(element, {
            removeItemButton: true
        });
    }
   
  
</script>

