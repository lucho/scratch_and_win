<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <!--progress bar-->


            <!--form panels-->
            <div class="row">
                <div class="col-12 col-lg-8 m-auto">
                @if (session('failure'))
                    <div class="mt-3 alert alert-primary alert-dismissible fade show" role="alert">
                        <span class="alert-text text-white">{{ session('failure') }}</span>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                        </button>
                    </div>
                    @endif
                    <form wire:submit.prevent="addFile" class="multisteps-form__form mb-8">
                        <!--single form panel-->
                        <div class="card multisteps-form__panel p-3 border-radius-xl bg-white js-active" data-animation="FadeIn">
                            <h5 class="font-weight-bolder">{{ __('Cargue Masivo') }}</h5>
                            <div class="multisteps-form__content">
                                <div wire:loading>
                                    Cargando archivo...
                                </div>
                                <div class="row mt-3">

                                    <!-- Excel -->
                                    <div class="row">
                                        <div class="col-12">
                                            <label>{{ __('Archivo') }}</label>

                                            <input type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" wire:model="upload" class="multisteps-form__input form-control" />

                                        </div>
                                        <div>
                                            @error('upload') <div class="text-danger text-xs mt-3">{{ $message }} @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Excel -->
                                    <div class="button-row d-flex mt-4">
                                        <button class="btn bg-gradient-dark ms-auto mb-0" type="submit" title="Send">{{ __('Cargar') }}</button>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--   Core JS Files   -->

<script src="../../../assets/js/plugins/choices.min.js"></script>
<script src="../../../assets/js/plugins/flatpickr.min.js"></script>
<script src="../../../assets/js/plugins/dropzone.min.js"></script>
<script>
    if (document.getElementById('choices-multiple-remove-button3')) {
        var element = document.getElementById('choices-multiple-remove-button3');
        const example = new Choices(element, {
            removeItemButton: true
        });
    }
    if (document.getElementById('choices-multiple-remove-button4')) {
        var element = document.getElementById('choices-multiple-remove-button4');
        const example = new Choices(element, {
            removeItemButton: true
        });
    }
    if (document.querySelector('.datetimepicker')) {
        flatpickr('.datetimepicker', {
            allowInput: true
        }); // flatpickr
    }
</script>