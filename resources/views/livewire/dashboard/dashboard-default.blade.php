<div class="container-fluid py-4">
    <div class="row">
        <div class="col-6 d-flex ms-auto">
           
            <!-- <div class="dropdown ms-3">
                <button class="btn bg-gradient-dark dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-bs-toggle="dropdown" aria-expanded="false">
                    Today
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="">
                    <li><a class="dropdown-item" href="#">Yesterday</a></li>
                    <li><a class="dropdown-item" href="#">Last 7 days</a></li>
                    <li><a class="dropdown-item" href="#">Last 30 days</a></li>
                </ul>
            </div> -->
        </div>
    </div>
    <div class="row">
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-8">
                            <div class="numbers">
                                <p class="text-sm mb-0 text-capitalize font-weight-bold">Clientes</p>
                                <h5 class="font-weight-bolder mb-0">
                                    {{$usersTotal}}
                                    <!-- <span class="text-success text-sm font-weight-bolder">+55%</span> -->
                                </h5>
                            </div>
                        </div>
                        <div class="col-4 text-end">
                            <div class="icon icon-shape bg-gradient-dark shadow text-center border-radius-md">
                                <i class="ni ni-circle-08 text-lg opacity-10" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-8">
                            <div class="numbers">
                                <p class="text-sm mb-0 text-capitalize font-weight-bold">Ó. registradas</p>
                                <h5 class="font-weight-bolder mb-0">
                                    {{$registered}}
                                    <!-- <span class="text-success text-sm font-weight-bolder">+3%</span> -->
                                </h5>
                            </div>
                        </div>
                        <div class="col-4 text-end">
                            <div class="icon icon-shape bg-gradient-dark shadow text-center border-radius-md">
                                <i class="ni ni-world text-lg opacity-10" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-8">
                            <div class="numbers">
                                <p class="text-sm mb-0 text-capitalize font-weight-bold">Ó. en procesó</p>
                                <h5 class="font-weight-bolder mb-0">
                                    {{$inProcess}}
                                    <!-- <span class="text-danger text-sm font-weight-bolder">-2%</span> -->
                                </h5>
                            </div>
                        </div>
                        <div class="col-4 text-end">
                            <div class="icon icon-shape bg-gradient-dark shadow text-center border-radius-md">
                                <i class="ni ni-watch-time text-lg opacity-10" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6">
            <div class="card">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-8">
                            <div class="numbers">
                                <p class="text-sm mb-0 text-capitalize font-weight-bold">Ó.finalizadas</p>
                                <h5 class="font-weight-bolder mb-0">
                                    {{$completed}}
                                    <!-- <span class="text-success text-sm font-weight-bolder">+5%</span> -->
                                </h5>
                            </div>
                        </div>
                        <div class="col-4 text-end">
                            <div class="icon icon-shape bg-gradient-dark shadow text-center border-radius-md">
                                <i class="ni ni-image text-lg opacity-10" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header pb-0 p-3">
                    <h6 class="mb-0">Ventas</h6>
                    <div class="d-flex align-items-center">
                        <span class="badge badge-md badge-dot me-4">
                            <i class="bg-primary"></i>
                            <span class="text-dark text-xs">Ventas por mes</span>
                        </span>
                        <!-- <span class="badge badge-md badge-dot me-4">
                            <i class="bg-dark"></i>
                            <span class="text-dark text-xs">Referral</span>
                        </span>
                        <span class="badge badge-md badge-dot me-4">
                            <i class="bg-info"></i>
                            <span class="text-dark text-xs">Social media</span>
                        </span> -->
                    </div>
                </div>
                <div class="card-body p-3">
                    <div class="chart">
                        <canvas id="chart-line" class="chart-canvas" height="300px"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <div class="d-flex align-items-center">
                        <h6 class="mb-0">Usuarios registrados por mes</h6>
                        <button type="button"
                            class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto"
                            data-bs-toggle="tooltip" data-bs-placement="bottom"
                            title="estos son los usuarios que se han registrado este año, separados por mes.">
                            <i class="fas fa-info"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body p-4">
                    <div class="row">
                        <div class="col-4 text-center">
                            <div class="chart">
                                <canvas id="chart-doughnut" class="chart-canvas" height="200px"></canvas>
                            </div>
                            <!-- <a class="btn btn-sm bg-gradient-secondary mt-4">See all referrals</a> -->
                        </div>
                        <div class="col-4">
                            <div class="table-responsive">
                                <table class="table align-items-center mb-0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/1.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Enero</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$januaryUser}} </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/2.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Febrero</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$februaryUser}} </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/3.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Marzo</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$marchUser}} </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/4.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Abril</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$aprilUser}} </span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/5.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Mayo</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$mayUser}} </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/6.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Junio</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$juneUser}} </span>
                                            </td>
                                        </tr>
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="table-responsive">
                                <table class="table align-items-center mb-0">
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/7.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Julio</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$julyUser}} </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/8.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Agosto</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$augustUser}} </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/9.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Septiembre</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$septemberUser}} </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/10.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Octubre</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$octoberUser}} </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/11.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Noviembre</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$novemberUser}} </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div>
                                                        <img src="../../assets/company/meses/12.png"
                                                            class="avatar avatar-sm me-2">
                                                    </div>
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Diciembre</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span class="text-xs font-weight-bold">{{$decemberUser}} </span>
                                            </td>
                                        </tr>
                                        
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
     
        
    </div>
</div>


 <script src="../../assets/js/plugins/chartjs.min.js"></script>
 <script>
   var ctx1 = document.getElementById("chart-line").getContext("2d");
   var ctx2 = document.getElementById("chart-doughnut").getContext("2d");

   var gradientStroke1 = ctx1.createLinearGradient(0, 230, 0, 50);

   gradientStroke1.addColorStop(1, 'rgba(203,12,159,0.2)');
   gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
   gradientStroke1.addColorStop(0, 'rgba(203,12,159,0)'); //purple colors

   var gradientStroke2 = ctx1.createLinearGradient(0, 230, 0, 50);

   gradientStroke2.addColorStop(1, 'rgba(20,23,39,0.2)');
   gradientStroke2.addColorStop(0.2, 'rgba(72,72,176,0.0)');
   gradientStroke2.addColorStop(0, 'rgba(20,23,39,0)'); //purple colors

   // Line chart
   new Chart(ctx1, {
     type: "line",
     data: {
       labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre","Octubre","Noviembre","Diciembre"],
       datasets: [{
           label: "Ventas por mes",
           tension: 0.4,
           borderWidth: 0,
           pointRadius: 2,
           pointBackgroundColor: "#cb0c9f",
           borderColor: "#cb0c9f",
           borderWidth: 3,
           backgroundColor: gradientStroke1,
           data: [@json($january),
           @json($february),
           @json($march),
           @json($april),
           @json($may),
           @json($june),
           @json($july),
           @json($august),
           @json($september),
           @json($october),
           @json($november),
           @json($december)
        ],
           maxBarThickness: 6
         },
        // {
        //    label: "Referral",
        //    tension: 0.4,
        //    borderWidth: 0,
        //    pointRadius: 2,
        //    pointBackgroundColor: "#3A416F",
        //    borderColor: "#3A416F",
        //    borderWidth: 3,
        //    backgroundColor: gradientStroke2,
        //    data: [30, 90, 40, 140, 290, 290, 340, 230, 400],
        //    maxBarThickness: 6
        //  },
        //  {
        //    label: "Direct",
        //    tension: 0.4,
        //    borderWidth: 0,
        //    pointRadius: 2,
        //    pointBackgroundColor: "#17c1e8",
        //    borderColor: "#17c1e8",
        //    borderWidth: 3,
        //    backgroundColor: gradientStroke2,
        //    data: [40, 80, 70, 90, 30, 90, 140, 130, 200],
        //    maxBarThickness: 6
        //  },
       ],
     },
     options: {
       responsive: true,
       maintainAspectRatio: false,
       plugins: {
         legend: {
           display: false,
         }
       },
       interaction: {
         intersect: false,
         mode: 'index',
       },
       scales: {
         y: {
           grid: {
             drawBorder: false,
             display: true,
             drawOnChartArea: true,
             drawTicks: false,
             borderDash: [5, 5]
           },
           ticks: {
             display: true,
             padding: 10,
             color: '#9ca2b7'
           }
         },
         x: {
           grid: {
             drawBorder: false,
             display: true,
             drawOnChartArea: true,
             drawTicks: true,
             borderDash: [5, 5]
           },
           ticks: {
             display: true,
             color: '#9ca2b7',
             padding: 10
           }
         },
       },
     },
   });


   // Doughnut chart
   new Chart(ctx2, {
     type: "doughnut",
     data: {
        labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre","Octubre","Noviembre","Diciembre"],
       datasets: [{
         label: "Usuarios registrados",
         weight: 9,
         cutout: 60,
         tension: 0.9,
         pointRadius: 2,
         borderWidth: 2,
         backgroundColor: ['#2152ff', '#3A416F', '#f53939', '#a8b8d8', '#cb0c9f',"#3393FF","#36FF33","#FF33FF","#12086D","#EC8210","#9210EC","#ECEC10"],
         data: [@json($januaryUser),
           @json($februaryUser),
           @json($marchUser),
           @json($aprilUser),
           @json($mayUser),
           @json($juneUser),
           @json($julyUser),
           @json($augustUser),
           @json($septemberUser),
           @json($octoberUser),
           @json($novemberUser),
           @json($decemberUser)
        ],
         fill: false
       }],
     },
     options: {
       responsive: true,
       maintainAspectRatio: false,
       plugins: {
         legend: {
           display: false,
         }
       },
       interaction: {
         intersect: false,
         mode: 'index',
       },
       scales: {
         y: {
           grid: {
             drawBorder: false,
             display: false,
             drawOnChartArea: false,
             drawTicks: false,
           },
           ticks: {
             display: false
           }
         },
         x: {
           grid: {
             drawBorder: false,
             display: false,
             drawOnChartArea: false,
             drawTicks: false,
           },
           ticks: {
             display: false,
           }
         },
       },
     },
   });
 </script>