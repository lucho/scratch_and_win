<div>
    <div class="container-fluid my-3 py-3 d-flex flex-column">
        <div class="row mb-5 justify-content-center align-items-center">
            <div class="col-9">
                <!-- Card Profile -->
                <div class="card card-body" id="profile">
                    <div class="row justify-content-center align-items-center">
                        <div class="col-auto">
                            <div class="avatar avatar-xl position-relative">
                                <div>
                                    <img src="/assets/company/reporte/reporte_dafault.png" alt="Profile Photo">
                                </div>
                            </div>
                        </div>
                        <div class="col-auto my-auto">
                            <div class="h-100">

                                <p class="mb-0 font-weight-bold text-sm">
                                    {{ __('Reporte / Órdenes') }}
                                </p>
                            </div>
                        </div>
                        <div class="col-auto ms-auto">
                            <div class="form-check form-switch">
                                <!-- <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" checked
                                    onchange="visible()"> -->
                                <label class="form-check-label" for="flexSwitchCheckDefault">
                                    <small id="profileVisibility">
                                        <!-- {{ __('Switch to invisible') }} -->
                                    </small>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    @if (session('succes'))
                    <div class="mt-3 alert alert-primary alert-dismissible fade show" role="alert">
                        <span class="alert-icon text-white"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text text-white">{{ session('succes') }}</span>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if (session('failure'))
                    <div class="mt-3 alert alert-primary alert-dismissible fade show" role="alert">
                        <span class="alert-text text-white">{{ session('failure') }}</span>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                        </button>
                    </div>
                    @endif
                </div>
                <!-- Card Basic Info -->
                <div class="card mt-4" id="basic-info">
                    <div class="card-header">
                        <h5>Filtros</h5>
                    </div>
                    <div class="card-body pt-0">
                        <form wire:submit.prevent="export" action="#" method="POST">
                            <!-- Fechas -->
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>{{ __('Fecha de inicio') }}</label>
                                    <div class="@error('start_date')has-danger @enderror ">
                                        <input wire:model="start_date" class="multisteps-form__input form-control @error('start_date')is-invalid @enderror datetimepicker" type="text" placeholder="Fecha de inicio" />
                                    </div>
                                    @error('start_date') <div class="text-danger text-xs">
                                        {{ $message }}
                                    </div>@enderror
                                </div>
                                <div class="col-sm-6">
                                    <label>{{ __('Fecha de finalizacion') }}</label>
                                    <div class="@error('end_date')has-danger @enderror ">
                                        <input wire:model="end_date" class="multisteps-form__input form-control @error('end_date')is-invalid @enderror datetimepicker" type="text" placeholder="Fecha de finalizacion" />
                                    </div>
                                    @error('end_date') <div class="text-danger text-xs">
                                        {{ $message }}
                                    </div>@enderror
                                </div>
                            </div>
                            <!-- /Fechas -->
                            <!-- Tipo de Estados -->
                            <div class="row mt-3">
                                <div wire:ignore>
                                    <label class="col-12 mt-3 mt-sm-0">{{ __('Tipo de cupón') }}</label>
                                    <div class=" @error('statusSelect') has-danger @enderror">
                                        <select wire:model="statusSelect" class="multisteps-form__select form-control @error('statusSelect') is-invalid @enderror" name="choices-multiple-remove-button3">
                                            <option selected value="">{{ __('elegir') }}</option>
                                            @foreach ($orderStatus as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @error('statusSelect') <div class="text-danger text-xs text-xs">
                                    {{ $message }}
                                </div> @enderror
                            </div>
                            <!-- /Tipo de Estados -->
                            <div class="">
                                <button type="submit" class="btn bg-gradient-dark btn-sm float-end mt-6 mb-0">{{ __('Exportar') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="../../../assets/js/plugins/flatpickr.min.js"></script>
<script>
    if (document.querySelector('.datetimepicker')) {
        flatpickr('.datetimepicker', {
            allowInput: true
        }); // flatpickr
    }
</script>