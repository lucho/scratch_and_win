<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <!--progress bar-->

            <!--form panels-->
            <div class="row">
                <div class="col-12 col-lg-8 m-auto">
                    <div class="card text-center">
                        <div class="card-header">
                            Gracias por jugar
                        </div>
                    </div>
                    <form wire:submit.prevent="addDonation" class="multisteps-form__form mb-8">
                        <!--single form panel-->
                        <div class="card multisteps-form__panel p-3 border-radius-xl bg-white js-active"
                            data-animation="FadeIn">
                            <h5 class="font-weight-bolder">{{ __('Registrar Donación') }}</h5>
                            <div class="multisteps-form__content">
                                <div class="row mt-3">
                                    <!-- Nombre -->
                                    <div class="col-12 col-sm-6">
                                        <label>{{ __('Nombre Jugador') }}</label>
                                        <div class="@error('name')has-danger @enderror">
                                            <input wire:model="name"
                                                class="multisteps-form__input form-control @error('name')is-invalid @enderror"
                                                type="text" placeholder="Nombre completo" />
                                        </div>
                                        @error('name') <div class="text-danger text-xs">
                                                {{ $message }}
                                        </div>@enderror
                                    </div>
                                    <!-- /Nombre -->

                                    <!-- telefono -->
                                    <div class="col-12 col-sm-6">
                                        <label>{{ __('Teléfono') }}</label>

                                        <div class="@error('phone')has-danger @enderror">
                                            <input wire:model="phone"
                                                class="multisteps-form__input form-control @error('phone')is-invalid @enderror"
                                                type="number" placeholder="Teléfono o celular" />
                                        </div>
                                        @error('phone') <div class="text-danger text-xs">
                                                {{ $message }}
                                        </div>@enderror
                                    </div>
                                    <!-- /telefono -->


                                    <!-- Email -->
                                    <div class="col-12 col-sm-6">
                                        <label>{{ __('Email') }}</label>

                                        <div class="@error('email')has-danger @enderror">
                                            <input wire:model="email"
                                                class="multisteps-form__input form-control @error('email')is-invalid @enderror"
                                                type="email" placeholder="Teléfono o celular" />
                                        </div>
                                        @error('email') <div class="text-danger text-xs">
                                                {{ $message }}
                                        </div>@enderror
                                    </div>
                                    <!-- /Email -->
                                    <div class="col-12 col-sm-6">
                                        <div class="card-body">
                                            <h5 class="card-title">Valor a donar:
                                                {{ number_format($totalValue, 2) }}</h5>
                                            <p class="card-text">Número de cartones: {{ implode(",", $this->tickets) }}</p>
                                        </div>

                                    </div>



                                    <!-- Imagen -->
                                    <div class="row">
                                        <div class="avatar avatar-xl position-relative">
                                            <div>
                                                <label>{{ __('Imagen') }}</label>

                                                <label for="file-input"
                                                    class="btn btn-sm btn-icon-only bg-gradient-light position-absolute bottom-0 end-0 mb-n2 me-n2">
                                                    <i class="fa fa-pen top-0" data-bs-toggle="tooltip"
                                                        data-bs-placement="top" title="" aria-hidden="true"
                                                        data-bs-original-title="Imagen" aria-label="Imagen"></i><span
                                                        class="sr-only">{{ 'Imagen' }}</span>
                                                </label>

                                                <input type="file" accept="image/*" wire:model="upload" id="file-input"
                                                    class="d-none">
                                                <span class="h-12 w-12 rounded-full overflow-hidden bg-gray-100">
                                                    @if ($upload)
                                                        <img src="{{ $upload->temporaryUrl() }}" alt="Profile Photo">
                                                    @else
                                                        <img src="/assets/img/placeholder.jpg" alt="Profile Photo">
                                                    @endif
                                                </span>
                                            </div>
                                            <div>
                                                @error('upload') <div class="text-danger text-xs mt-3">
                                                    {{ $message }} @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Imagen -->
                                    <div class="button-row d-flex mt-4">

                                        <button class="btn bg-gradient-dark ms-auto mb-0" type="submit"
                                            title="Send">{{ __('Registrar') }}</button>
                                    </div>
                                    <br>
                                    <a href="{{ route('player') }}" class="btn btn-primary"> No Donar, volver a jugar</a>

                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--   Core JS Files   -->

<script src="../../../assets/js/plugins/choices.min.js"></script>
<script src="../../../assets/js/plugins/flatpickr.min.js"></script>
<script src="../../../assets/js/plugins/dropzone.min.js"></script>
<script>
    if (document.getElementById('choices-multiple-remove-button3')) {
        var element = document.getElementById('choices-multiple-remove-button3');
        const example = new Choices(element, {
            removeItemButton: true
        });
    }
    if (document.getElementById('choices-multiple-remove-button4')) {
        var element = document.getElementById('choices-multiple-remove-button4');
        const example = new Choices(element, {
            removeItemButton: true
        });
    }
    if (document.querySelector('.datetimepicker')) {
        flatpickr('.datetimepicker', {
            allowInput: true
        }); // flatpickr
    }
</script>
