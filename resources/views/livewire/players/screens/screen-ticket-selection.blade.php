<style>
    :root {
        --w: calc(70vw / 6);
        --h: calc(70vh / 4);
    }

    * {
        transition: all 0.5s;
    }

    body {
        padding: 0;
        margin: 0;
        -webkit-perspective: 1000;
        background: powderblue;
        min-height: 100vh;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center;
        font-family: calibri;
    }

    div {
        display: inline-block;
    }

    .area-tarjeta,
    .tarjeta,
    .cara {
        cursor: pointer;
        width: var(--w);
        min-width: 100px;
        height: var(--h);
    }

    .tarjeta {
        position: relative;
        transform-style: preserve-3d;
        animation: iniciar 5s;
    }

    .cara {
        position: absolute;
        backface-visibility: hidden;
        box-shadow: inset 0 0 0 5px white;
        font-size: 500%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .trasera {
        background-color: lightcyan;
        transform: rotateY(180deg);
    }

    .superior {
        background: linear-gradient(orange, darkorange);
    }

    .nuevo-juego {
        cursor: pointer;
        background: linear-gradient(orange, darkorange);
        padding: 20px;
        border-radius: 50px;
        border: white 5px solid;
        font-size: 130%;
    }

    .donar {
        cursor: pointer;
        background: linear-gradient(rgb(115, 226, 64), rgb(0, 255, 85));
        padding: 20px;
        border-radius: 50px;
        border: white 5px solid;
        font-size: 130%;
    }

    @keyframes iniciar {

        20%,
        90% {
            transform: rotateY(180deg);
        }

        0%,
        100% {
            transform: rotateY(0deg);
        }
    }

</style>

<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <main class="main-content  mt-0">
                <form action="{{ url('screen-selected-group') }}" method="GET">
                    <div class="card" style="width: 18rem;">
                        <ul class="list-group list-group-flush">
                            <input type="text" name="Iv" id="InputvalorDonar" style="display:none;">
                            <input type="text" name="iC" id="InputnumeroCartones" style="display:none;">
                            <li class="list-group-item" wire:model="valorDonar" id="valorDonar">Valor a Donar: </li>
                            <li class="list-group-item" id="numeroCartones">Número de cartones: </li>
                        </ul>
                    </div>
                    <br>
                    <div id="tablero">
                    </div>
                    <br>
                    <div class="donar" type="submit" id="donar" style="display:none;">
                        <button class="btn bg-gradient-dark ms-auto mb-0" type="submit" title="Send">{{ __('Donar') }}</button>
                    </div>

                    <br>
                    <div class="nuevo-juego" onclick="generarTablero()">
                        Nuevo Juego
                    </div>

                </form>
            </main>
        </div>
    </div>
</div>

<!-- JS -->
<!-- parte lógica -->
<script>
    let ticketsGlobal = @json($tickets);

    let iconos = [];
    let selecciones = [];
    let valorTotal = 0;
    let cartonesGlobales = [];

    generarTablero()

    function cargarIconos() {
        iconos = [
            '<i class="fas fa-star"></i>',
            '<i class="far fa-star"></i>',
            '<i class="fas fa-star-of-life"></i>',
            '<i class="fas fa-star-and-crescent"></i>',
            '<i class="fab fa-old-republic"></i>',
            '<i class="fab fa-galactic-republic"></i>',
            '<i class="fas fa-sun"></i>',
            '<i class="fas fa-stroopwafel"></i>',
            '<i class="fas fa-dice"></i>',
            '<i class="fas fa-chess-knight"></i>',
            '<i class="fas fa-chess"></i>',
            '<i class="fas fa-dice-d20"></i>',
        ]
    }

    function generarTablero() {
        var tickets = @json($tickets);
        cargarIconos()
        selecciones = []
        let tablero = document.getElementById("tablero")
        let tarjetas = []
        let contador = 0;
        let interaccion = 0;
        for (let i = 0; i < (tickets.length * 2); i++) {
            contador++;
            tarjetas.push(`
            <div class="area-tarjeta" onclick="seleccionarTarjeta(${i},${tickets[interaccion].id})">
                <div class="tarjeta" id="tarjeta${i}" value="${tickets[interaccion].id}">
                    <div class="card cara trasera" id="trasera${i}">
                            ${iconos[0]}
                         <div class="">
                           <h5 class="">$ ${new Intl.NumberFormat().format(tickets[interaccion].donation)} </h5>
                           <p class="">Número: ${tickets[interaccion].id}</p>
                         </div>

                    </div>
                    <div class="cara superior">
                        <i class="far fa-question-circle"></i>
                    </div>
                </div>
            </div>        
            `)
            if (i % 2 == 1) {
                iconos.splice(0, 1)
            }
            if (i) {
                if (contador >= 2) {
                    interaccion++;
                    contador = 0;
                }
            }
        }
        tarjetas.sort(() => Math.random() - 0.5)
        tablero.innerHTML = tarjetas.join(" ")
    }

    function seleccionarTarjeta(i, ticketID = null) {
        let tarjeta = document.getElementById("tarjeta" + i)
        if (tarjeta.style.transform != "rotateY(180deg)") {
            tarjeta.style.transform = "rotateY(180deg)"
            selecciones.push(i)
        }
        if (selecciones.length == 2) {
            deseleccionar(selecciones, ticketID)
            selecciones = []
        }
    }

    function deseleccionar(selecciones, ticketID = null) {
        setTimeout(() => {
            let trasera1 = document.getElementById("trasera" + selecciones[0])
            let trasera2 = document.getElementById("trasera" + selecciones[1])
            if (trasera1.innerHTML != trasera2.innerHTML) {
                let tarjeta1 = document.getElementById("tarjeta" + selecciones[0])
                let tarjeta2 = document.getElementById("tarjeta" + selecciones[1])
                tarjeta1.style.transform = "rotateY(0deg)"
                tarjeta2.style.transform = "rotateY(0deg)"
            } else {
                trasera1.style.background = "plum"
                trasera2.style.background = "plum"
                agregarValorTotal(ticketID);
            }
        }, 1000);
    }



    function agregarValorTotal(ticketID) {
        ticketsGlobal.forEach(element => {
            if (element.id == ticketID) {
                valorTotal = (valorTotal + parseFloat(element.donation));
                cartonesGlobales.push(ticketID)
            }

        });
        var x = document.getElementById("donar");
        var valor = document.getElementById("valorDonar");
        var cartones = document.getElementById("numeroCartones");

        document.getElementById("InputvalorDonar").value = valorTotal;
        document.getElementById("InputnumeroCartones").value = cartonesGlobales;

        valor.innerHTML = "Valor a Donar: $ " + new Intl.NumberFormat().format(valorTotal);
        cartones.innerHTML = "Número de cartones: " + cartonesGlobales.toString();

        if (x.style.display === "none") {
            x.style.display = "block";
        }

    }
</script>
