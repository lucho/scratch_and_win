<?php

use Illuminate\Support\Facades\Route;

use App\Http\Livewire\Dashboard\DashboardDefault;




use App\Http\Livewire\Authentication\Error\Error404;
use App\Http\Livewire\Authentication\Error\Error500;

use App\Http\Livewire\Authentication\Lock\LockBasic;
use App\Http\Livewire\Authentication\Lock\LockCover;
use App\Http\Livewire\Authentication\Lock\LockIllustration;



use App\Http\Livewire\Auth\Login;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', Login::class)->name('login');

Route::get('/login', Login::class)->name('login');

Route::middleware('auth')->group(function () {
    /**
     * Menú
     */
    Route::get('/home', DashboardDefault::class)->name('home');
  

  

    Route::get('/authentication-error404', Error404::class)->name('404');
    Route::get('/authentication-error500', Error500::class)->name('500');

    Route::get('/authentication-lock-basic', LockBasic::class)->name('lock-basic');
    Route::get('/authentication-lock-cover', LockCover::class)->name('lock-cover');
    Route::get('/authentication-lock-illustration', LockIllustration::class)->name('lock-illustration');

   
    
});

/*****************************************
 *         Rutas Personalizadas          *
 ****************************************/

 /**
  * Campañas
  */
 include_once __DIR__ . '/web/campaigns/campaign/routes.php';
 include_once __DIR__ . '/web/campaigns/ticket/routes.php';

 /**
  * Jugadores
  */
  include_once __DIR__ . '/web/players/player/routes.php';
  include_once __DIR__ . '/web/players/screen/routes.php';

