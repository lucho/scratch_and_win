<?php

use App\Http\Livewire\Campaigns\Campaign\CampaignCreate;
use App\Http\Livewire\Campaigns\Campaign\CampaignEdit;
use App\Http\Livewire\Campaigns\Campaign\CampaignList;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function () {
    /**
     * Cupones
     */
    Route::get('/campaign-create', CampaignCreate::class)->name('campaign-create');
    Route::get('/campaign-edit/{id}', CampaignEdit::class)->name('campaign-edit');
    Route::get('/campaign-list', CampaignList::class)->name('campaign-list');


});
