<?php

use App\Http\Livewire\Campaigns\Ticket\TicketCreate;
use App\Http\Livewire\Campaigns\Ticket\TicketEdit;
use App\Http\Livewire\Campaigns\Ticket\TicketList;
use App\Http\Livewire\Campaigns\Ticket\TicketMassiveLoad;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function () {
    /**
     * Cupones
     */
    Route::get('/campaign-ticket-create', TicketCreate::class)->name('campaign-ticket-create');
    Route::get('/campaign-ticket-edit/{id}', TicketEdit::class)->name('campaign-ticket-edit');
    Route::get('/campaign-ticket-list', TicketList::class)->name('campaign-ticket-list');
    Route::get('/campaign-ticket-massive-load', TicketMassiveLoad::class)->name('campaign-ticket-massive-load');

});
