<?php

use App\Http\Livewire\Players\Screens\ScreenTicketSelection;
use App\Http\Livewire\Players\Screens\ScreenSelectedGroup;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/screen-ticket-selection', ScreenTicketSelection::class)->name('screen-ticket-selection');
Route::get('/screen-selected-group', ScreenSelectedGroup::class)->name('screen-selected-group');
